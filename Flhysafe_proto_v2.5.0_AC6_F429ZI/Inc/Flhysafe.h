/**
  ******************************************************************************
  * @file           : Flhysafe.h
  * @brief          : Header for Flhysafe.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
 **/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FLHYSAFE_H__
#define __FLHYSAFE_H__

#include<stdio.h>
#include<stdint.h>
#include<string.h>
#include <float.h>

/* System declarations  ---------------------------------------------------------*/

#define NB_I_BRANCH 12

#define FPGA_ADD1  0x60000000 // Bank 1 - SRAM 1 -> NE1
#define FPGA_ADD2  0x64000000 // Bank 1 - SRAM 2 -> NE2

/* Data Block declarations  ---------------------------------------------------------*/

/* Block address */
#define DB_ADD_ADC1 0x00
#define DB_ADD_ADC2 0x01
#define DB_ADD_ADC3 0x02
#define DB_ADD_CAN1 0x03
#define DB_ADD_CAN2 0x04

#define  DB_FLAG_SIZE 5  /* Data Block Stack Size */


/* Data Block size (uint32_t) of each block */
#define DB_SIZE_ADC1 18
#define DB_SIZE_ADC2 6
#define DB_SIZE_ADC3 4
#define DB_SIZE_CAN1 1
#define DB_SIZE_CAN2 2


/*  functions Declaration */
void DB_init();
void DB_read(uint32_t BD_Add,  uint32_t *DB_Data);
void DB_write(uint32_t DB_Add, uint32_t *DB_Data);

void Calc_adc_values(uint32_t *adc_Val);


#endif /* __FLHYSAFE_H__ */

/************************ (C) COPYRIGHT C *****END OF FILE****/

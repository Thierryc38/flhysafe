/**
  ******************************************************************************
  * @file           : Flhysafe_test.h
  * @brief          : Header for Flhysafe.c file.
  *                   This file contains the test defines of the application.
  ******************************************************************************
 **/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FLHYSAFE_TEST_H__
#define __FLHYSAFE_TEST_H__

#include "Flhysafe.h"


/* Data Block test  ---------------------------------------------------------*/


void DB_test();


#endif /* __FLHYSAFE_TEST_H__ */

/************************ (C) COPYRIGHT C *****END OF FILE****/

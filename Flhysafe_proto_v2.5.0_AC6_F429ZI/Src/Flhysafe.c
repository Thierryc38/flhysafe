/**
  ******************************************************************************
  * @file           : Flhysafe.c
  * @brief          : Application program body
  ******************************************************************************
 **/

/* Includes ------------------------------------------------------------------*/
#include "Flhysafe.h"

/* Data Block variables  */
uint8_t DB_FLAG_ADC1 = 0;  /* ADC1 flag/stack pointer */
uint8_t DB_FLAG_ADC2 = 0;  /* ADC2 flag/stack pointer */
uint8_t DB_FLAG_ADC3 = 0;  /* ADC3 flag/stack pointer */
uint8_t DB_FLAG_CAN1 = 0;  /* CAN1 flag/stack pointer */
uint8_t DB_FLAG_CAN2 = 0;  /* CAN2 flag/stack pointer */

uint32_t DB_DATA_ADC1[DB_FLAG_SIZE][DB_SIZE_ADC1] = {[0 ... DB_FLAG_SIZE-1][0 ... DB_SIZE_ADC1-1] = 0};  /* ADC1 flag bloc */
uint32_t DB_DATA_ADC2[DB_FLAG_SIZE][DB_SIZE_ADC1] = {[0 ... DB_FLAG_SIZE-1][0 ... DB_SIZE_ADC2-1] = 0};  /* ADC2 flag bloc */
uint32_t DB_DATA_ADC3[DB_FLAG_SIZE][DB_SIZE_ADC1] = {[0 ... DB_FLAG_SIZE-1][0 ... DB_SIZE_ADC3-1] = 0};  /* ADC3 flag bloc */
uint32_t DB_DATA_CAN1[DB_FLAG_SIZE][DB_SIZE_CAN1] = {[0 ... DB_FLAG_SIZE-1][0 ... DB_SIZE_CAN1-1] = 0};  /* CAN1 flag bloc */
uint32_t DB_DATA_CAN2[DB_FLAG_SIZE][DB_SIZE_CAN2] = {[0 ... DB_FLAG_SIZE-1][0 ... DB_SIZE_CAN2-1] = 0};  /* CAN2 flag bloc */

/* Measurement variables  ---------------------------------------------------------*/
uint32_t Ibranch[NB_I_BRANCH] = {[0 ... NB_I_BRANCH-1] = 0}; // in mV
uint32_t VFC  = 0; // in mV
uint32_t Vbus = 0; // in mV
uint32_t Ibus = 0; // in  mV
uint32_t Tmod = 0; // in  mV


/*  DATA Block function access */
void DB_init()
{

	/*Initialise Data Blocks Flags */
	DB_FLAG_ADC1 = 0;
	DB_FLAG_ADC2 = 0;
	DB_FLAG_ADC3 = 0;
	DB_FLAG_CAN1 = 0;
	DB_FLAG_CAN2 = 0;

}


void DB_read(uint32_t  DB_Add, uint32_t *DB_Data)
{
uint8_t DB_FLAG = 0;  /* the next flag/stack pointer value */
uint8_t I = 0;  /* Data pointer */

	switch (DB_Add)
	{
	case DB_ADD_ADC1:
		DB_FLAG = DB_FLAG_ADC1;
		for (I=0; I<DB_SIZE_ADC1; I++)
			DB_Data[I] = DB_DATA_ADC1[DB_FLAG][I];
		break;

	case DB_ADD_ADC2:
		DB_FLAG = DB_FLAG_ADC2;
		for (I=0; I<DB_SIZE_ADC2; I++)
			DB_Data[I] = DB_DATA_ADC2[DB_FLAG][I];
		break;

	case DB_ADD_ADC3:
		DB_FLAG = DB_FLAG_ADC3;
		for (I=0; I<DB_SIZE_ADC3; I++)
			DB_Data[I] = DB_DATA_ADC3[DB_FLAG][I];
		break;

	case DB_ADD_CAN1:
		DB_FLAG = DB_FLAG_CAN1;
		for (I=0; I<DB_SIZE_CAN1; I++)
			DB_Data[I] = DB_DATA_CAN1[DB_FLAG][I];
		break;

	case DB_ADD_CAN2:
		DB_FLAG = DB_SIZE_CAN2;
		for (I=0; I<DB_SIZE_CAN2; I++)
			DB_Data[I] = DB_DATA_CAN2[DB_FLAG][I];
		break;

	}
}

void DB_write(uint32_t DB_Add, uint32_t *DB_Data)
{
uint8_t DB_FLAG_NEXT = 0;  /* the next flag/stack pointer value */
uint8_t I = 0;             /* Data pointer */

	switch (DB_Add)
	{
	case DB_ADD_ADC1:
		DB_FLAG_NEXT = (DB_FLAG_ADC1 < DB_FLAG_SIZE-1) ? DB_FLAG_ADC1+1 : 0;
		for (I=0; I<DB_SIZE_ADC1; I++)
			 DB_DATA_ADC1[DB_FLAG_NEXT][I] = DB_Data[I];
		DB_FLAG_ADC1 = DB_FLAG_NEXT;
		break;

	case DB_ADD_ADC2:
		DB_FLAG_NEXT = (DB_FLAG_ADC2 < DB_FLAG_SIZE-1) ? DB_FLAG_ADC2+1 : 0;
		for (I=0; I<DB_SIZE_ADC2; I++)
			 DB_DATA_ADC2[DB_FLAG_NEXT][I] = DB_Data[I];
		DB_FLAG_ADC2 = DB_FLAG_NEXT;
		break;

	case DB_ADD_ADC3:
		DB_FLAG_NEXT = (DB_FLAG_ADC3 < DB_FLAG_SIZE-1) ? DB_FLAG_ADC3+1 : 0;
		for (I=0; I<DB_SIZE_ADC3; I++)
			 DB_DATA_ADC3[DB_FLAG_NEXT][I] = DB_Data[I];
		DB_FLAG_ADC3 = DB_FLAG_NEXT;
		break;

	case DB_ADD_CAN1:
		DB_FLAG_NEXT = (DB_FLAG_CAN1 < DB_FLAG_SIZE-1) ? DB_FLAG_CAN1+1 : 0;
		for (I=0; I<DB_SIZE_CAN1; I++)
			 DB_DATA_CAN1[DB_FLAG_NEXT][I] = DB_Data[I];
		DB_FLAG_CAN1 = DB_FLAG_NEXT;
		break;

	case DB_ADD_CAN2:
		DB_FLAG_NEXT = (DB_FLAG_CAN2 < DB_FLAG_SIZE-1) ? DB_FLAG_CAN2+1 : 0;
		for (I=0; I<DB_SIZE_CAN2; I++)
			 DB_DATA_CAN2[DB_FLAG_NEXT][I] = DB_Data[I];
		DB_FLAG_CAN2 = DB_FLAG_NEXT;
		break;

	}

}

/* Conversion macros */
#define Calc_Val_Ibranch(ADC_VAL) (uint32_t)((((float)ADC_VAL * 3.3 / 1024.0 ) - 0.0) * 1000.0) // in mV // 3V @ 40 A
#define Calc_Val_VFC(ADC_VAL)     (uint32_t)((((float)ADC_VAL * 3.3 / 1024.0 ) - 0.0) * 1000.0) // in mV //2.82V @ 350 V
#define Calc_Val_Vbus(ADC_VAL)    (uint32_t)((((float)ADC_VAL * 3.3 / 1024.0 ) - 0.0) * 1000.0) // in mV // 2.79V @ 660 V
#define Calc_Val_Ibus(ADC_VAL)    (uint32_t)((((float)ADC_VAL * 3.3 / 1024.0 ) - 0.0) * 1000.0) // in mV // DELETED !!!
#define Calc_Val_Tmod(ADC_VAL)    (uint32_t)((((float)ADC_VAL * 3.3 / 1024.0 ) - 0.0) * 1000.0) // in mV // TBD


void Calc_adc_values(uint32_t *adc_Val)
{
	// Reorder the adc_values
	Ibranch[0]  = Calc_Val_Ibranch(adc_Val[0]);
	Ibranch[1]  = Calc_Val_Ibranch(adc_Val[3]);
	Ibranch[2]  = Calc_Val_Ibranch(adc_Val[6]);
	Ibranch[3]  = Calc_Val_Ibranch(adc_Val[9]);
	Ibranch[4]  = Calc_Val_Ibranch(adc_Val[12]);
	Ibranch[5]  = Calc_Val_Ibranch(adc_Val[15]);
	Ibranch[6]  = Calc_Val_Ibranch(adc_Val[1]);
	Ibranch[7]  = Calc_Val_Ibranch(adc_Val[4]);
	Ibranch[8]  = Calc_Val_Ibranch(adc_Val[7]);
	Ibranch[9]  = Calc_Val_Ibranch(adc_Val[10]);
	Ibranch[10] = Calc_Val_Ibranch(adc_Val[13]);
	Ibranch[11] = Calc_Val_Ibranch(adc_Val[16]);

	VFC  = Calc_Val_VFC(adc_Val[2]);
	Vbus = Calc_Val_Vbus(adc_Val[5]);
	Ibus = Calc_Val_Ibus(adc_Val[8]);
	Tmod = Calc_Val_Tmod(adc_Val[11]);

}

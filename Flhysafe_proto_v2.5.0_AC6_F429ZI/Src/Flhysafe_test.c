/**
  ******************************************************************************
  * @file           : Flhysafe_test.c
  * @brief          : Application program body
  ******************************************************************************
 **/

/* Includes ------------------------------------------------------------------*/
#include "Flhysafe_test.h"

uint32_t TEST_DB_DATA0_ADC1[DB_SIZE_ADC1] = {[0 ... DB_SIZE_ADC1-1] = 0};  /* ADC1 flag bloc */
uint32_t TEST_DB_DATA1_ADC1[DB_SIZE_ADC1] = {1, 2, 3, 4, 5, 6 };
uint32_t TEST_DB_DATA2_ADC1[DB_SIZE_ADC1] = {21, 22, 23, 24, 25, 26 };
uint32_t TEST_DB_DATA3_ADC1[DB_SIZE_ADC1] = {41, 42, 43, 44, 45, 46 };

uint32_t TEST_DB_DATA0_CAN1[DB_SIZE_CAN1] = {[0 ... DB_SIZE_CAN1-1] = 0};  /* CAN1 flag bloc */
uint32_t TEST_DB_DATA1_CAN1[DB_SIZE_CAN1] = {101};  /* CAN1 flag bloc */

uint32_t TEST_DB_DATA0_CAN2[DB_SIZE_CAN2] = {[0 ... DB_SIZE_CAN2-1] = 0};  /* CAN2 flag bloc */
uint32_t TEST_DB_DATA1_CAN2[DB_SIZE_CAN2] = {201, 202};  /* CAN2 flag bloc */


void DB_test()
{
	DB_write(DB_ADD_ADC1, TEST_DB_DATA1_ADC1);
	DB_write(DB_ADD_CAN1, TEST_DB_DATA1_CAN1);
	DB_write(DB_ADD_CAN2, TEST_DB_DATA1_CAN2);

	DB_read(DB_ADD_ADC1, TEST_DB_DATA0_ADC1);
	DB_read(DB_ADD_CAN1, TEST_DB_DATA0_CAN1);
	DB_read(DB_ADD_CAN2, TEST_DB_DATA0_CAN2);

	DB_write(DB_ADD_ADC1, TEST_DB_DATA2_ADC1);
	DB_read(DB_ADD_ADC1, TEST_DB_DATA0_ADC1);

	DB_write(DB_ADD_ADC1, TEST_DB_DATA3_ADC1);
	DB_read(DB_ADD_ADC1, TEST_DB_DATA0_ADC1);

}
